package prueba_tecnica;

import java.util.LinkedList;

// Clase que representa a la entidad producto. Considere que cada producto tiene un stock ilimitado.
public class Producto {
	private int codigo;
	private int precio;
	private String nombre;
	private String categoria;
	
	
	// Constructor
	public Producto (int codigo, int precio, String nombre, String categoria){
		this.codigo = codigo;
		this.precio = precio;
		this.nombre = nombre;
		this.categoria = categoria;
	}

	
	/////// GETTERS Y SETTERS ///////
	
	public int getCodigo() {
		return codigo;
	}

	
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	
	public int getPrecio() {
		return this.precio;
	}

	
	public void setPrecio(int precio) {
		this.precio = precio;
	}

	
	public String getNombre() {
		return this.nombre;
	}

	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	
	public String getCategoria() {
		return this.categoria;
	}

	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}
