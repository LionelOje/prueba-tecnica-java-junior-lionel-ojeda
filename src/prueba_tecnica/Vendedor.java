package prueba_tecnica;

import java.util.LinkedList;

// Clase que representa a la entidad vendedor
public class Vendedor {
	private int codigo;
	private int sueldo;
	private String nombre;
	private int cantidadProductosVendidos;

	
	// Constructor
	public Vendedor (int codigo, int sueldo, String nombre) {
		this.codigo = codigo;
		this.sueldo = sueldo;
		this.nombre = nombre;
		this.cantidadProductosVendidos = 0;
	}

	
	/////// GETTERS Y SETTERS ///////
	
	public int getCodigo() {
		return this.codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	public int getSueldo() {
		return sueldo;
	}


	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getCantidadProductosVendidos() {
		return this.cantidadProductosVendidos;
	}


	public void setCantidadProductosVendidos(int cantidadProductosVendidos) {
		this.cantidadProductosVendidos = cantidadProductosVendidos;
	}
	
}
