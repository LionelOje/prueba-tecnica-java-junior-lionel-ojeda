package prueba_tecnica;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

// Esta clase me agrupa en una lista todas las ventas correspondientes a cada producto que se van haciendo. Una venta corresponde a un solo tipo de producto (la cantidad que se quiera), luego el usuario puede ir agregando otros tipos de productos (otras ventas). Por lo tanto, la totalidad de los elementos que conforman esta lista, corresponde a lo que seria una orden de compra, y es sobre ese monto total que se calcula la comision del vendedor. Osea esta lista seria como el ticket del supermercado, que te registra los tipos de producto que llevaste, la cantidad y el precio de la unidad x1, x2 etc.   
public class RegistroVentasProductos {
	private LinkedList <VentaProducto> registroVentasProductos;
	private boolean yaRegistroUnaVentaDeProducto = false;
	private MetodosInputsValidacionesExepciones metodosInputsValidacionesExepciones =  new MetodosInputsValidacionesExepciones();
	private boolean condicion1;
	private boolean condicion2;
	private boolean continuarPrograma;
	private String textDescripcionErrorCodigoProducto = "\n" + "***El codigo de producto que ingreso no existe, ingrese un numero existente o presione 0 para volver al menu principal y ver los productos disponibles";;

	
	/////// GETTERS Y SETTERS  ///////

	public RegistroVentasProductos(){
		this.registroVentasProductos = new LinkedList<VentaProducto>();
	}

	
	public LinkedList<VentaProducto> getProductosVendidos() {
		return this.registroVentasProductos;
	}


	public void setVentas(LinkedList<VentaProducto> productosVendidos) {
		this.registroVentasProductos = productosVendidos;
	}
	
		
	public boolean getYaRegistroUnaVentaDeProducto() {
		return this.yaRegistroUnaVentaDeProducto;
	}


	private void setYaRegistroUnaVentaDeProducto(boolean yaRegistroUnaVentaDeProducto) {
		this.yaRegistroUnaVentaDeProducto = yaRegistroUnaVentaDeProducto;
	}

	///////////
	
	
	// Metodo para generar la venta de un producto (el primero que se vende) 
	public void generarVentaProducto (LinkedList<Producto> productos, LinkedList <Vendedor> vendedores){
		int precioProducto = 0 ;
		int valorVentaProducto = 0;
		String nombreProducto = "";
		String nombreVendedor = "";
		String textIngresoCodigoProducto = "\n" + "Ingrese el codigo del producto que vendio:";
		String textIngresoCodigoVendedor = "Ingrese el codigo de identificacion del vendedor";
		String textIngresoCantidadProductos = "Ingrese la cantidad de productos que vendio";
		
		System.out.println("\n" + "\n");
		System.out.println("*** En caso de querer volver al menu principal presione 0 en cualquiera de las opciones ***");
		this.continuarPrograma = false;
		this.condicion1 = false;
		this.condicion2 = true;
		String textDescripcionErrorCodigoEmpleado = "\n" + "***El codigo de empleado que ingreso no existe, ingrese un numero existente o presione 0 para volver al menu principal y ver los productos disponibles";
		
		 	// Pide al usuario que ingrese por pantalla el codigo del producto que se vendio
			// Se asegura que el codigo exista en la lista 
			// Maneja la exepcion de ingresar un String como int
			// Si presiono 0 vuelve al menu principal
		int codigoProducto =33;
		codigoProducto = this.metodosInputsValidacionesExepciones.validacionYexepcionIngresoCodigoProducto(this.condicion1, this.condicion2, this.continuarPrograma, this.textDescripcionErrorCodigoProducto, codigoProducto, textIngresoCodigoProducto, productos);
		if (codigoProducto==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla la cantidad de productos vendidos
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		int cantidadProductos =33;
		cantidadProductos= this.metodosInputsValidacionesExepciones.exepcionIngresoUsuarioInt(cantidadProductos, textIngresoCantidadProductos);
		if (cantidadProductos==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla el codigo del vendedor que hiso la venta
		// Se asegura que el codigo exista en la lista 
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		int codigoVendedor = 33;
		codigoVendedor = this.metodosInputsValidacionesExepciones.validacionYexepcionIngresoCodigoEmpleado(this.condicion1, this.condicion2, this.continuarPrograma, textDescripcionErrorCodigoEmpleado, codigoVendedor, textIngresoCodigoVendedor, vendedores);
		if (codigoVendedor==0){
			return;
		}
			
		// Recorre la lista de productos, y una ves encontrado el producto vendido, me guarda en variables esos datos que luego se van a utilizar para agregar una venta a la lista y para mostrar por pantalla. 
		for(Producto pro : productos) {
			if(codigoProducto==pro.getCodigo()){
				nombreProducto=pro.getNombre();
				precioProducto = pro.getPrecio();
				valorVentaProducto = precioProducto * cantidadProductos;
			}
		}	
		
		// Recorre la lista de productos, y una ves encontrado el producto vendido, me guarda en variables esos datos que luego se van a utilizar para agregar una venta a la lista y para mostrar por pantalal. 
		for(Vendedor ven : vendedores) {
			if(codigoVendedor==ven.getCodigo()){
				nombreVendedor = ven.getNombre();
				
				//Me acutualiza la cantidad de productos que vendio el vendedor
				ven.setCantidadProductosVendidos(ven.getCantidadProductosVendidos()+ cantidadProductos);
			}
		}
			
		System.out.println("\n" + "Nombre del producto vendido: " + " " + nombreProducto);
		System.out.println ("Cantidad de productos de este tipo vendidos:" + " " + cantidadProductos);
		System.out.println ("Precio unidad producto:" + " " + precioProducto + "$");
		System.out.println ("Valor total de los productos de este tipo elejidos:" + " " + valorVentaProducto + "$");
		System.out.println ("Nombre del vendedor:" + " " + nombreVendedor);
		
		// Una ves pasada todas las validaciones y exepciones me guarda una nueva venta de producto nueva
		this.registroVentasProductos.add(new VentaProducto (codigoProducto, codigoVendedor, cantidadProductos, valorVentaProducto, precioProducto, nombreVendedor, nombreProducto));	
		
		// Esta flag la pone en true asi la proxima venta utilzo el meteodo de generarOtraVenta
		setYaRegistroUnaVentaDeProducto(true);
	}
	
	
	// Este metodo funciona igual que el anterior. Con la diferencia de que no pide un codigo de vendedor, sino que utiliza el que se registro en la primera venta. Esto es asi para que el usuario no ingrese otro codigo de vendedor ya que todas las ventas que se realizan en la ejecucion del programa pertenecen a unico vendedor.
	public void generarOtraVentaDeProducto (LinkedList<Producto> productos, LinkedList <Vendedor> vendedores){
		int precioProducto = 0;
		int valorVentaProducto = 0;
		String nombreProducto = "";
		String nombreVendedor = "";
		String textIngresoCodigoProducto = "\n" + "Ingrese el codigo del producto que vendio";
		String textIngresoCantidadProductos = "Ingrese la cantidad de productos que vendio";
		
		
		int codigoVendedor = 0;
		for (VentaProducto vent : this.registroVentasProductos){
			codigoVendedor = vent.getCodigoVendedor();
		}
	
		System.out.println("\n" + "\n");
		System.out.println("*** En caso de querer volver al menu principal presione 0 en cualquiera de las opciones ***");
		
		int codigoProducto =33;
		codigoProducto = this.metodosInputsValidacionesExepciones.validacionYexepcionIngresoCodigoProducto(this.condicion1, this.condicion2, this.continuarPrograma, this.textDescripcionErrorCodigoProducto, codigoProducto, textIngresoCodigoProducto, productos);
		if (codigoProducto==0){
			return;
		}
		
		int cantidadProductos =33;
		cantidadProductos= this.metodosInputsValidacionesExepciones.exepcionIngresoUsuarioInt(cantidadProductos, textIngresoCantidadProductos);
		if (cantidadProductos==0){
			return;
		}
		
		for(Producto pro : productos) {
			if(codigoProducto==pro.getCodigo()){
				nombreProducto = pro.getNombre();
				precioProducto = pro.getPrecio();
				valorVentaProducto = precioProducto * cantidadProductos;
			}
		}	
				for(Vendedor ven : vendedores) {
					if(codigoVendedor==ven.getCodigo()){
						nombreVendedor = ven.getNombre();
						ven.setCantidadProductosVendidos(ven.getCantidadProductosVendidos()+ cantidadProductos);
					}
				}
				
		System.out.println("\n" + "Nombre del producto vendido: " + " " + nombreProducto);
		System.out.println ("Cantidad de productos de este tipo vendidos:" + " " + cantidadProductos);
		System.out.println ("Precio unidad producto:" + " " + precioProducto + "$");
		System.out.println ("Valor total de los productos de este tipo elejidos:" + " " + valorVentaProducto + "$");
		System.out.println ("Nombre del vendedor:" + " " + nombreVendedor);
		this.registroVentasProductos.add(new VentaProducto ( codigoProducto, codigoVendedor, cantidadProductos, valorVentaProducto, precioProducto, nombreVendedor, nombreProducto));
	}
}
