package prueba_tecnica;

import java.util.LinkedList;
import java.util.Scanner;

// Esta clase me agrupa todos los vendedores de la tienda en una lista y me permite trabajarlos en conjunto.
public class RegistroVendedores {
	private LinkedList <Vendedor> vendedores;
	private boolean flagSiRealizoVenta = false;
	private MetodosInputsValidacionesExepciones metodosValidacionesExepciones =  new MetodosInputsValidacionesExepciones();
	
	
	// Constructor
	public RegistroVendedores(){
		this.vendedores = new LinkedList<Vendedor>();
		this.vendedores.add(new Vendedor (1, 20000, "Scaloni"));
		this.vendedores.add(new Vendedor (2, 25000, "Homero"));
		this.vendedores.add(new Vendedor (3, 22000, "Apu"));
	}


	/////// GETTERS Y SETTERS DE LA LISTA QUE CONTIENE LOS PRODUCTOS ///////

	public LinkedList<Vendedor> getVendedores() {
		return this.vendedores;
	}


	public void setVendedores(LinkedList<Vendedor> vendedores) {
		this.vendedores = vendedores;
	}
	
	
	// Metodo para agregar un vendedor a la lista de productos, luego de haber pasado por las validaciones
	public void agregarVendedor (){
		String textIngresoCodigo ="\n" + "\n" + "\n" + "Ingrese el codigo del vendedor que desea agregar, o presione 0 para volver al menu principal";
		String textIngresoNombre = "Ingrese el nombre del vendedor que desea agregar, o escriba salir para volver al menu principal";
		String textIngresoSueldo = "Ingrese el sueldo del vendedor que desea agregar o presione 0 para volver al menu principal";
		boolean continuarPrograma = false;
		boolean condicion1 = true;
		boolean condicion2 = false;
		String textDescripcionError = "\n" + "***Ya existe un vendedor con ese codigo, ingrese un numero que no este registrado o presione 0 para volver al menu principal y ver los productos disponibles";
		int sueldo = 33;
		int codigoVendedor = 33;
		String nombre = "";
		
		// Pide al usuario que ingrese por pantalla el codigo
		// Se asegura que el codigo no exista en la lista para no repetirlo
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		codigoVendedor = this.metodosValidacionesExepciones.validacionYexepcionIngresoCodigoEmpleado(condicion1, condicion2, continuarPrograma, textDescripcionError, codigoVendedor, textIngresoCodigo, vendedores);
		if (codigoVendedor==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla el sueldo
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		sueldo = this.metodosValidacionesExepciones.exepcionIngresoUsuarioInt(sueldo, textIngresoSueldo);
		if (sueldo==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla el nombre del vendedor
		// Verifica que el ingrese sean solo letras
		// Si ingreso Salir vuelve al menu principal
		nombre = this.metodosValidacionesExepciones.verificaSiElIngresoSonSoloLetras(nombre, textIngresoNombre);
		if (nombre.equals("Salir")){
			return;
		}
		
		// Cuando pasa todas las validaciones agrega el vendedor a la lista y lo muestra por pantalla
		this.vendedores.add(new Vendedor (codigoVendedor, sueldo, nombre));
		System.out.println( "\n" + "Vendedor agregado:" + "\n" + "Codigo vendedor:" + codigoVendedor + " | " + "Nombre vendedor:" + nombre + " | " + "Sueldo vendedor:" + sueldo + "$");
	}
	
	
	// Calcula la comision del vendedor que esta vendiendo
	public void calcularComision (LinkedList <VentaProducto> registroVentasProductos){
		int valorDeTodasLasVentas = 0;
		int codigoVendedor = 0; 
		int comisionVendedor = 0;
		
		// Recorre la lista del registro de los productos vendidos
		// Coloca la flag de si ya realizo una venta en true. Si todavia no se vendio ningun producto no va a recorrer nada por que la lista con las ventas va a etar vacia, por lo tanto esta flag queda en false
		// Si hay elementos en la lista me guarda en esta variable el valor de los productos vendidos hasta el momento
		// Me guarda en una variable el codigo del vendedor que realizo la venta 
		for (VentaProducto ven : registroVentasProductos){
			this.flagSiRealizoVenta = true;
			valorDeTodasLasVentas += ven.getValorVentaProducto();
			codigoVendedor = ven.getCodigoVendedor();
		}
		
		// Si la flag esta en false osea que no realizo ninguna venta, me lo muestra por pantalla
		if(!this.flagSiRealizoVenta){
			System.out.print( "\n" +  "\n" +  "\n" + "***Todavia no se vendio ningun producto" + "\n");
			return;
		}
		
		// Me ubica en la lista de vendedores cual es el vendedor que realizo la venta para utilizar los atributos de su clase y calcular la comision, segun corresponde.
		for (Vendedor ven : this.vendedores){
			if (codigoVendedor == ven.getCodigo()){
				if (ven.getCantidadProductosVendidos()<=2){ //En los metodos de generarVentas cada ves que se realiza una venta de un producto se van guardando en ese atributo del vendedor la cantidad, por lo que lo puedo usar para calcular la comision
					comisionVendedor = (valorDeTodasLasVentas * 5) / 100; 
				}
				else{
					comisionVendedor = (valorDeTodasLasVentas * 10) / 100;
				} 
				
				System.out.println("\n" + "\n" + "Valor Total de la compra hecha por el comprador:" + " " + valorDeTodasLasVentas + "$" + "\n" + "Comision Obtenida por el vendedor:" + " " + comisionVendedor + "$" + "\n" + "Nombre del vendedor:" + " " + ven.getNombre()+ "\n" + "codigo del vendedor:" + " " + ven.getCodigo());
			}
		}	
	}
	
	// Me muestra todos los vendedores por pantalla
	public void mostrar (){
		for(Vendedor ven : this.vendedores){
			System.out.println("Codigo vendedor:" + ven.getCodigo()+ " | " + "Nombre vendedor:" + ven.getNombre()+ " | " + "Sueldo vendedor:" + ven.getSueldo());
		}
	}
}

