package prueba_tecnica;

import java.util.LinkedList;
import java.util.Scanner;

// Esta clase me agrupa todos los productos de la tienda en una lista y me permite trabajarlas en conjunto.
public class RegistroProductos {
	private LinkedList <Producto> productos;
	private MetodosInputsValidacionesExepciones metodosValidacionesExepciones =  new MetodosInputsValidacionesExepciones();
	private boolean condicion1;
	private boolean condicion2;
	private boolean continuarPrograma;
	private String textDescripcionError;
	
	
	// Constructor 
	public RegistroProductos (){
		this.productos = new LinkedList<Producto>();
		productos.add(new Producto (1, 90000, "Iphone", "Celulares"));
		productos.add(new Producto (2, 20000, "Lgmod", "Celulares"));
		productos.add(new Producto (3, 5000, "Parlante", "Audio"));
		productos.add(new Producto (4, 6500, "Grabador", "Audio"));
		productos.add(new Producto (5, 1447, "Teclado", "Computacion"));
		productos.add(new Producto (6, 12222, "Gabinete", "Computacion"));
	}

	/////// GETTERS Y SETTERS DE LA LISTA QUE CONTIENE LOS PRODUCTOS///////
	
	public LinkedList<Producto> getProductos() {
		return this.productos;
	}

	public void setProductos(LinkedList<Producto> productos) {
		this.productos = productos;
	}
	
	/////////
	
	
	// Metodo para agregar un producto a la lista de productos, luego de haber pasado por las validaciones
	public void agregarProducto (){
		String textIngresoCodigo = "\n" + "\n" + "\n" + "Ingrese el codigo del producto que desea agregar, o presione 0 para volver al menu principal";
		String textIngresoNombre = "Ingrese el nombre del producto que desea agregar, o escriba salir para volver al menu principal ";
		String textIngresoCategoria = "Ingrese la categoria del producto que desea agregar, o escriba salir para volver al menu principal";
		String textIngresoPrecio = "Ingrese el precio del producto que desea agregar, o presione 0 para volver al menu principal";
		
		int codigoProducto =33;
		int precioProducto =33;
		String nombreProducto = "";
		String categoriaProducto = "";
		this.continuarPrograma = false;
		this.condicion1 = true;
		this.condicion2 = false;
		this.textDescripcionError = "\n" + "***Cada producto tiene un codigo de identificacion unico, ingrese otro codigo o presione 0 para volver al menu principal";
		
		// Pide al usuario que ingrese por pantalla el codigo
		// Se asegura que el codigo no exista en la lista para no repetirlo
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		codigoProducto = this.metodosValidacionesExepciones.validacionYexepcionIngresoCodigoProducto(this.condicion1, this.condicion2, this.continuarPrograma, this.textDescripcionError, codigoProducto, textIngresoCodigo, this.productos);
		if (codigoProducto==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla el precio
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		precioProducto = this.metodosValidacionesExepciones.exepcionIngresoUsuarioInt(precioProducto, textIngresoPrecio);
		if (precioProducto==0){
			return;
		}
		
		// Pide al usuario que ingrese por pantalla el nombre de producto
		// Verifica que el ingrese sean solo letras
		// Si ingreso Salir vuelve al menu principal
		nombreProducto = this.metodosValidacionesExepciones.verificaSiElIngresoSonSoloLetras(nombreProducto, textIngresoNombre);
		if (nombreProducto.equals("Salir")){
			return;
		}

		// Pide al usuario que ingrese por pantalla el nombre de producto
		// Verifica que el ingreso sean solo letras
		// Si ingreso Salir vuelve al menu principal
		categoriaProducto = this.metodosValidacionesExepciones.verificaSiElIngresoSonSoloLetras(categoriaProducto, textIngresoCategoria);
		if (categoriaProducto.equals("Salir")){
			return;
		}
		
		// Cuando pasa todas las validaciones agrega el producto a la lista y lo muestra por pantalla
		this.productos.add(new Producto (codigoProducto, precioProducto, nombreProducto, categoriaProducto));
		System.out.println( "\n" + "Producto agregado:" + "\n" + "Codigo producto:" + codigoProducto + " | " + "Nombre producto:" + nombreProducto+ " | " + "Precio producto:" + precioProducto + "$" + " | " + "Categoria producto:" + categoriaProducto);
	}
	
	
	//Metodo que muestra por pantalla todos los productos de la lista
	public void mostrarTodosLosProductos (){
		System.out.println("\n"); 
		for(Producto pro : this.productos){
			System.out.println("\n" + "Codigo producto:" + pro.getCodigo()+ " | " + "Nombre producto:" + pro.getNombre()+ " | " + "Precio producto:" + pro.getPrecio()+ "$" + " | " + "Categoria producto:" + pro.getCategoria());
		}
	}

	
	// Filtra los productos de la lista segun el nombre ingresado por el usuario
	public void filtrarPorNombre (){
		String textoInputFiltro = "\n" + "Ingrese el nombre del producto que desea buscar:";
		
		System.out.println("\n" + "\n" + "\n" + "*** En caso de querer volver al menu principal presione 0 ***");
		
		// Pide al usuario que ingrese por pantalla el nombre de producto
		// Verifica que el ingrese sean solo letras
		// Se asegura de que el producto este en la lista
		// Si ingreso Salir vuelve al menu principal
		String filtro = "";
		filtro = this.metodosValidacionesExepciones.validacionFiltroNombreProducto(filtro, textoInputFiltro, this.productos);
		if (filtro.equals("Salir")){
			return;
		}
		
		// Si el filtro ingreasado pasa las validaciones y no escribio salir, lo busca en la lista y lo muestra por pantalla
		for(Producto pro : this.productos) {
			if(filtro.equals(pro.getNombre())){
				System.out.println( "\n" + "Codigo producto:" + pro.getCodigo()+ " | " + "Nombre producto:" + pro.getNombre()+ " | " + "Precio producto:" + pro.getPrecio()+ "$" + " | " + "Categoria producto:" + pro.getCategoria());
			}
		}
	}
	
	
	// Filtra los productos de la lista segun el nombre de categoria ingresada por el usuario
	public void filtrarPorCategoria (){
		String textoInputFiltro = "\n" +  "Ingrese la categoria del producto que desea buscar:";
		
		System.out.println("\n" + "\n" + "\n" + "*** En caso de querer volver al menu principal presione 0 ***");
		
		// Pide al usuario que ingrese por pantalla el nombre de categoria de producto
		// Verifica que el ingrese sean solo letras
		// Se asegura de que el producto este en la lista
		// Si ingreso Salir vuelve al menu principal
		String filtro = "";
		filtro = this.metodosValidacionesExepciones.validacionFiltroCategoriaProducto(filtro, textoInputFiltro, this.productos);
		if (filtro.equals("Salir")){
			return;
		}
	
		// Si el filtro ingreasado pasa las validaciones y no escribio salir, lo busca en la lista y lo muestra por pantalla
		for(Producto pro : this.productos) {
			if(filtro.equals(pro.getCategoria())){
				 System.out.println( "\n" + "Codigo producto:" + pro.getCodigo()+ " | " + "Nombre producto:" + pro.getNombre()+ " | " + "Precio producto:" + pro.getPrecio()+ "$" + " | " + "Categoria producto:" + pro.getCategoria());
			}
		}
	}
	
	
	// Filtra los productos de la lista segun el codigo ingresado por el usuario
	public void filtrarPorCodigo (){
		String textIngresoCodigoProducto = "\n" +  "Ingrese el codigo del producto que desea buscar:";
		
		System.out.println("\n" + "\n" +  "\n" + "*** En caso de querer volver al menu principal presione 0 ***");
		
		int codigoProducto =33;
		this.continuarPrograma = false;
		this.condicion1 = false;
		this.condicion2 = true;
		this.textDescripcionError = "\n" + "***El codigo de producto que ingreso no existe, ingrese un numero existente o presione 0 para volver al menu principal y ver los productos disponibles";
		
		// Pide al usuario que ingrese por pantalla el codigo
		// Se asegura que el codigo exista en la lista 
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		codigoProducto = this.metodosValidacionesExepciones.validacionYexepcionIngresoCodigoProducto(this.condicion1, this.condicion2, this.continuarPrograma, this.textDescripcionError, codigoProducto, textIngresoCodigoProducto, this.productos);
		if (codigoProducto==0){
			return;
		}
		
		// Si el filtro ingreasado pasa las validaciones y no escribio 0, lo busca en la lista y lo muestra por pantalla
		for (Producto pro : this.productos){
			if (codigoProducto==pro.getCodigo()){	
				System.out.println( "\n" + "Codigo producto:" + pro.getCodigo()+ " | " + "Nombre producto:" + pro.getNombre()+ " | " + "Precio producto:" + pro.getPrecio() + "$" + " | " + "Categoria producto:" + pro.getCategoria());
			}
		}
	}
	
	// Filtra los productos de la lista segun el precio ingresado por el usuario
	public void filtrarPorPrecio (){
		String textIngresoPrecioProducto = "\n" +  "Ingrese el precio del producto que desea buscar:";
		
		System.out.println("\n" + "\n" + "\n" + "*** En caso de querer volver al menu principal presione 0 ***");
		
		// Pide al usuario que ingrese por pantalla el precio
		// Se asegura que el codigo exista en la lista 
		// Maneja la exepcion de ingresar un String como int
		// Si presiono 0 vuelve al menu principal
		int precioProducto =33;
		precioProducto = this.metodosValidacionesExepciones.validacionYexepcionIngresoPrecioProducto(precioProducto, textIngresoPrecioProducto, this.productos);
		if (precioProducto==0){
			return;
		}
		
		// Si el filtro ingreasado pasa las validaciones y no escribio 0, lo busca en la lista y lo muestra por pantalla
		for (Producto pro : this.productos){
			if (precioProducto==pro.getPrecio()){	
				System.out.println( "\n" + "Codigo producto:" + pro.getCodigo()+ " | " + "Nombre producto:" + pro.getNombre()+ " | " + "Precio producto:" + pro.getPrecio() + "$" + " | " + "Categoria producto:" + pro.getCategoria());
			}
		}
	}
	
}


