package prueba_tecnica;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

// Esta clase me almacena los distintos metodos auxiliares, relacionados a los ingresos de datos, validaciones y exepciones.
public class MetodosInputsValidacionesExepciones {
	
	
	// Constructor vacio
	public MetodosInputsValidacionesExepciones(){}
	
	
	// Metodo para pedirle al usuario que ingrese un valor de tipo entero
	public int ingresoUsuarioInt (String textLoQueSeQuiereIngresar){
		Scanner lectura = new Scanner (System.in);

		System.out.println(textLoQueSeQuiereIngresar);
	
		int ingresoUser = lectura.nextInt();
		return ingresoUser;
	}

	
	// Metodo para pedirle al usuario que ingrese un valor de tipo String. Luego del ingreso, me pasa el valor a minuscula y la primera letra me la pasa a mayuscula 
	public String ingresoUsuarioString (String textLoQueSeQuiereIngresar){
		Scanner lectura = new Scanner (System.in);

		System.out.println(textLoQueSeQuiereIngresar);
	
		String ingresoUser = lectura.next();
		ingresoUser.toLowerCase();
		String ingresoConPrimeraLetraMayuscula = ingresoUser.toUpperCase().charAt(0)+ ingresoUser.substring(1, ingresoUser.length()).toLowerCase();;
		return ingresoConPrimeraLetraMayuscula;
	}

	
	// Metodo para verificar si el valor String ingresado son solo letras
	private boolean esSoloLetras(String valorIngresado){
		
		// Recorre la cadena letra por letra verificando si el caracter esta entre los caracteres ASCII que corresponde a letras
		for (int i = 0; i < valorIngresado.length(); i++)
		{
			char caracter = valorIngresado.toUpperCase().charAt(i);
			int valorASCII = (int)caracter;
			if (valorASCII != 165 && (valorASCII < 65 || valorASCII > 90))
				return false; 
		}
		return true;
	}
	
	
	// Le pide al usuario que ingrese un string y si no son solo letras me tira un mensaje de error, y vuelve a pedirle al usuario que ingrese un String. Esta accion se repite hasta que el ingreso sean solo letras   
	public String verificaSiElIngresoSonSoloLetras(String valorIngresado, String textDescripcionInput){
		boolean continuarPrograma = false;
		
		while(!continuarPrograma){
				
			valorIngresado = ingresoUsuarioString(textDescripcionInput);
			if (esSoloLetras(valorIngresado)){
				continuarPrograma = true;
			}
			else{
				System.out.println("\n" + "***Debe ingresar un valor de tipo texto (solo letras), o escriba salir para volver al menu principal");
			}
		}
		return valorIngresado;
	}
	
	
	// Le pide al usuario que ingrese un entero y si ingresa letras me maneja la exepcion mandando un mensaje de error por pantalla. Esta accion se repite hasta que el ingreso sea un entero 
	public int exepcionIngresoUsuarioInt(int valorIngresado, String textDescripcionInput){
		boolean continuarPrograma = false;
		
		while(!continuarPrograma){
			try{
				valorIngresado = ingresoUsuarioInt(textDescripcionInput);
				continuarPrograma = true;
			}catch(InputMismatchException ex){
				System.out.println("\n" + "***Debe ingresar un valor de tipo numerico, o presione 0 para volver al menu principal");
			}	
		}
		return valorIngresado;
	}
	
	
	// Funciona igual que el metodo anterior con la diferencia que cambia el mensaje de error, se que podria haber reutilizado el metodo anterior pero tenia que entregar la prueba y para hacer eso tenia que cambiar todos los metodos en donde habia usado el metodo anterior. En fin esto es una solucion rapida para que quede mas prolijo el metodo ejecucion.
	public int exepcionIngresoUsuarioEjecucion (int valorIngresado, String textDescripcionInput){
		boolean continuarPrograma = false;
		
		while(!continuarPrograma){
			try{
				valorIngresado = ingresoUsuarioInt(textDescripcionInput);
				continuarPrograma = true;
			}catch(InputMismatchException ex){
				System.out.println("\n" + "***Debe ingresar un valor de tipo numerico");
			}	
		}
		return valorIngresado;
	}
	
	
	// Metodo para la validacion del ingreso codigo del producto 
	public int validacionYexepcionIngresoCodigoProducto (boolean condicion1, boolean condicion2, boolean continuar, String textDescripcionError, int valorIngresado, String textDescripcionInput, LinkedList<Producto> productos){
		int valorIngresadoSinExepcion =33;
		
		// El ciclo se repite hasta que el dato este bien ingresado
		while (!continuar){
			continuar=condicion1;
			
			// Mediante este metodo se pide al usuario que ingrese un entero y me maneja la exepcion 
			valorIngresadoSinExepcion = exepcionIngresoUsuarioInt(valorIngresado, textDescripcionInput);
			
			// Si el valor ingresado es 0 retorna 0, lo que va a generar que se vuelva al menu principal
			if (valorIngresadoSinExepcion ==0){
				return valorIngresadoSinExepcion;
			}
			
			// Verifica que el codigo ingresado este en la lista
			// Esto me puede servir para detectar que el codigo ingresado existe o no existe. Eso va a depender del valor que se le asigne a condicion1 y condicion2, segun para lo que se lo quiera usar al metodo. Ej para agregar un producto tengo que verificar que no exista uno igual, para filtrar tengo que verificar que si exista
			for (Producto pro : productos){
				if (pro.getCodigo()==valorIngresadoSinExepcion){
					continuar = condicion2;
				}
				
			} // si continuar sigue siendo falso voy a mandar como mensaje de error que el producto ya existe o no segun el caso.
			if (!continuar){
				System.out.println(textDescripcionError);
			}
		}
		return valorIngresadoSinExepcion;
	}
	
	
	// Metodo para la validacion del ingreso del precio del producto
	public int validacionYexepcionIngresoPrecioProducto (int valorIngresado, String textDescripcionInput, LinkedList<Producto> productos){
		boolean continuarPrograma = false;
		int valorIngresadoSinExepcion =33;
		
		// El ciclo se repite hasta que el dato este bien ingresado
		while (!continuarPrograma){
			
			// Mediante este metodo se pide al usuario que ingrese un entero y me maneja la exepcion
			valorIngresadoSinExepcion = exepcionIngresoUsuarioInt(valorIngresado, textDescripcionInput);
			
			// Si el valor ingresado es 0 retorna 0, lo que va a generar que se vuelva al menu principal
			if (valorIngresadoSinExepcion ==0){
				return valorIngresadoSinExepcion;
			}
			
			for (Producto pro : productos){
				if (pro.getPrecio()==valorIngresadoSinExepcion){
					continuarPrograma = true;
				}
			}
			
			// si continuar sigue siendo falso voy a mandar como mensaje de error que el producto no existe
			if (!continuarPrograma){
				System.out.println("\n" + "***No hay ningun producto con ese precio, ingrese un precio existente o presione 0 para volver al menu principal y ver los productos disponibles");
			}
		}
		return valorIngresadoSinExepcion;
	}

	
	// Metodo para la validacion del ingreso codigo del empleado
	public int validacionYexepcionIngresoCodigoEmpleado (boolean condicion1, boolean condicion2, boolean continuar, String textDescripcionError, int valorIngresado, String textDescripcionInput, LinkedList<Vendedor> vendedores){
		
		int valorIngresadoSinExepcion =33;
		
		// El ciclo se repite hasta que el dato este bien ingresado
		while (!continuar){
			continuar=condicion1;
			
			// Mediante este metodo se pide al usuario que ingrese un entero y me maneja la exepcion
			valorIngresadoSinExepcion = exepcionIngresoUsuarioInt(valorIngresado, textDescripcionInput);
			
			// Si el valor ingresado es 0 retorna 0, lo que va a generar que se vuelva al menu principal
			if (valorIngresadoSinExepcion ==0){
				return valorIngresadoSinExepcion;
			}
			
			// Verifica que el codigo ingresado este en la lista
			// Esto me puede servir para detectar que el codigo ingresado existe o no existe. Eso va a depender del valor que se le asigne a condicion1 y condicion2, segun para lo que se lo quiera usar al metodo. Ej para agregar un producto tengo que verificar que no exista uno igual, para filtrar tengo que verificar que si exista
			for (Vendedor ven : vendedores){
				if (ven.getCodigo()==valorIngresadoSinExepcion){
					continuar = condicion2;
				}
			}
			
			// si continuar sigue siendo falso voy a mandar como mensaje de error que el vendedor ya existe o no segun el caso.
			if (continuar==false){
				System.out.println(textDescripcionError);
			}
		}
		return valorIngresadoSinExepcion;
	}
	
	
	// Metodo para la validacion del ingreso del nombre del producto
	public String validacionFiltroNombreProducto (String valorIngresado, String textDescripcionInput, LinkedList<Producto> productos){
		boolean continuarPrograma = false;
		valorIngresado = "";
		
		// El ciclo se repite hasta que el dato este bien ingresado
		while (!continuarPrograma){
			
			// Le pide al usuario que ingrese un string y verifica si son solo letras
			valorIngresado = verificaSiElIngresoSonSoloLetras(valorIngresado, textDescripcionInput);
			
			// Si el valor ingresado es "salir" retorna "salir", lo que va a generar que se vuelva al menu principal
			if (valorIngresado.equals("Salir")){
				return valorIngresado;
			}
			
			// Si el nombre del producto ingresado esta en la lista me pone a continuar en true lo que quiere decir que paso la validacion
			for (Producto pro : productos){
				if (pro.getNombre().equals(valorIngresado)){
					continuarPrograma = true;
				}
			}
			
			// si continuar sigue siendo falso voy a mandar como mensaje de error que el producto no existe
			if (continuarPrograma==false){
				System.out.println("\n" + "***El nombre de producto que ingreso no existe, ingrese un nombre existente o escriba salir para volver al menu principal y ver los productos disponibles");
			}
		}
		return valorIngresado;
	}
	
	
	// Metodo para la validacion del ingreso del nombre de la categoria del producto
	public String validacionFiltroCategoriaProducto (String valorIngresado, String textDescripcionInput, LinkedList<Producto> productos){
		boolean continuarPrograma = false;
		valorIngresado = "";
		
		// El ciclo se repite hasta que el dato este bien ingresado
		while (!continuarPrograma){
			
			// Le pide al usuario que ingrese un string y verifica si son solo letras
			valorIngresado = verificaSiElIngresoSonSoloLetras(valorIngresado, textDescripcionInput);
			
			// Si el valor ingresado es "salir" retorna "salir", lo que va a generar que se vuelva al menu principal
			if (valorIngresado.equals("Salir")){
				return valorIngresado;
			}
			
			// Si el nombre de categoria del producto ingresado esta en la lista, me pone a continuar en true lo que quiere decir que paso la validacion
			for (Producto pro : productos){
				if (pro.getCategoria().equals(valorIngresado)){
					continuarPrograma = true;
				}
			}
			
			// si continuar sigue siendo falso voy a mandar como mensaje de error que el producto no existe
			if (continuarPrograma==false){
				System.out.println("\n" + "***El nombre de categoria de producto que ingreso no existe, ingrese un nombre de categoria existente o escriba salir para volver al menu principal y ver los productos disponibles");
			}
		}
		return valorIngresado;
	}
}


