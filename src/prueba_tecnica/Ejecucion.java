package prueba_tecnica;

import java.util.InputMismatchException;
import java.util.Scanner;

// Clase utilizada para la ejecucion del programa
public class Ejecucion {
	RegistroProductos registroProductos = new RegistroProductos ();
	RegistroVendedores registroVendedores = new RegistroVendedores ();
	RegistroVentasProductos registroVentasProductos = new RegistroVentasProductos();
	boolean terminarEjecucionPrograma = false;
	private MetodosInputsValidacionesExepciones metodosInputsValidacionesExepciones =  new MetodosInputsValidacionesExepciones();
	
	
	// Constructor vacio
	public Ejecucion (){}
	
	
	// Metodo para mostrar todas las opciones disponibles 
	public void mostrarOpciones (String textSiEsElPrimerProductoVendido){
		System.out.println("\n" + "1 - Registrar" + " " + textSiEsElPrimerProductoVendido);
		System.out.println("2 - Filtrar producto por codigo");
		System.out.println("3 - Filtrar producto por nombre");
		System.out.println("4 - Filtrar producto por categoria");
		System.out.println("5 - Filtrar producto por precio");
		System.out.println("6 - Mostrar todos los productos disponibles");
		System.out.println("7 - Agregar producto");
		System.out.println("8 - Agregar Vendedor");
		System.out.println("9 - Calcular la comision que lleva hasta el momento");
		System.out.println("10 - Finalizar orden de compra y salir del programa");
	}
	
	
	// Metodo para la ultima parte del programa. Coloca los mensajes de despedida y muestra por pantalla los datos de la comision como un resumen final
	public void finalizarPrograma(){
		System.out.println("\n" + "\n" + "\n" + "******** GRACIAS POR UTILIZAR EL SISTEMA DE GESTION DE LA TIENDA CONSOUL ********");
		System.out.println("\n" + "\n" + "Resumen final:");
		registroVendedores.calcularComision(registroVentasProductos.getProductosVendidos());
		this.terminarEjecucionPrograma = true;
	}
	
	
	// Si ya se registro una venta, el texto de la opcion cambia
	public void detalleTextoOpcion1 (){
		if (registroVentasProductos.getYaRegistroUnaVentaDeProducto() == false){
			mostrarOpciones ("venta de producto");
		}
		else{
			mostrarOpciones ("otra venta de producto");
		}
	}
	
	
	// Aca va toda la ejecucion del programa
	public void ejecucionPrograma (){
		
		String textIngresoNumOpcion = "\n" + "Ingresar Opcion:";
		
		System.out.println("\n" + "******** BIENVENIDO AL SISTEMA DE GESTION DE LA TIENDA CONSOUL ********");
		
		// Para que finalize el programa se debe presionar 10 que es la opcion que me pone esta variable en true
		while (terminarEjecucionPrograma == false){
			System.out.println("\n" + "\n" + "Ingrese el numero de la accion que desea realizar");
			int numeroDeOpcion = 33;
			
			// Me muestra todas las opciones y la opcion1 con el texto de registrar venta o registrar otra venta
			detalleTextoOpcion1();
			
			// Le pide al usuario que ingrese el numero de opcion y maneja la exepcion de haber ingresado letras cuando se pedia un entero, 	
			numeroDeOpcion = this.metodosInputsValidacionesExepciones.exepcionIngresoUsuarioEjecucion(numeroDeOpcion, textIngresoNumOpcion);
			
			// Este swicht es el que me maneja el menu de opciones 
			// La explicacion de cada metodo esta en su clase correspondiente 
			switch(numeroDeOpcion) {
			
			case 1:
				// Si todavia no se genero una venta me ejecuta el metodo generarVentaProducto, y si ya se realizo una venta me ejecuta el metodo generarOtraVentaDeProducto. Esto es asi ya que ambos metodos presentan diferencias
				if (registroVentasProductos.getYaRegistroUnaVentaDeProducto() == false){
					registroVentasProductos.generarVentaProducto(registroProductos.getProductos(), registroVendedores.getVendedores());
				}
				else{
					registroVentasProductos.generarOtraVentaDeProducto(registroProductos.getProductos(), registroVendedores.getVendedores());
				}
				break;
			case 2:
				registroProductos.filtrarPorCodigo();
				break;
			case 3:
				registroProductos.filtrarPorNombre();
				break;
			case 4:
				registroProductos.filtrarPorCategoria();
				break;
			case 5:
				registroProductos.filtrarPorPrecio();
				break;
			case 6:
				registroProductos.mostrarTodosLosProductos();
				break;
			case 7:
				registroProductos.agregarProducto();
				break;
			case 8:
				registroVendedores.agregarVendedor();
				break;		
			case 9:
				registroVendedores.calcularComision(registroVentasProductos.getProductosVendidos());
				break;		
			case 10:
				finalizarPrograma();
				break;	
			default:
				System.out.println("\n" + "\n" + "***La opcion que elejiste no existe, Aca tenes nuevamente el menu de opciones :)");
			}	
		}		
	}
}
