package prueba_tecnica;

// Clase que representa a la entidad puente VentaProducto. Esta me vincula a las entidades vendedor y producto, y me brinda informacion necesaria para el funcionamiento del programa
public class VentaProducto {
	private int codigoVenta;
	private int codigoProducto;
	private int codigoVendedor;
	private String nombreProducto;
	private String nombreVendedor;
	private int valorUnidadProducto;
	private int cantidadProductosVendidos;
	private int valorVentaProducto;
	
	
	// Constructor
	public VentaProducto ( int codigoProducto, int codigoVendedor, int cantidadProductosVendidos, int valorVentaProducto, int valorUnidadProducto, String nombreVendedor, String nombreProducto) {
		this.codigoProducto = codigoProducto;
		this.codigoVendedor = codigoVendedor;
		this.cantidadProductosVendidos = cantidadProductosVendidos; 
		this.valorVentaProducto = valorVentaProducto ;
		this.valorUnidadProducto = valorUnidadProducto;
		this.nombreProducto = nombreProducto;
		this.nombreVendedor = nombreVendedor;
		
	}

	
	/////// GETTERS Y SETTERS ///////

	public int getCodigoVenta() {
		return this.codigoVenta;
	}


	public void setCodigoVenta(int codigoVenta) {
		this.codigoVenta = codigoVenta;
	}


	public int getCodigoProducto() {
		return this.codigoProducto;
	}


	public void setCodigoProducto(int codigoProducto) {
		this.codigoProducto = codigoProducto;
	}


	public int getCodigoVendedor() {
		return this.codigoVendedor;
	}


	public void setCodigoVendedor(int codigoVendedor) {
		this.codigoVendedor = codigoVendedor;
	}


	public int getCantidadProductosVendidos() {
		return this.cantidadProductosVendidos;
	}


	public void setCantidadProductosVendidos(int cantidadProductosVendidos) {
		this.cantidadProductosVendidos = cantidadProductosVendidos;
	}


	public int getValorVentaProducto() {
		return this.valorVentaProducto;
	}


	public void setValorVentaProducto(int valorVentaProducto) {
		this.valorVentaProducto = valorVentaProducto;
	}


	public int getValorUnidadProducto() {
		return this.valorUnidadProducto;
	}


	public void setValorUnidadProducto(int valorUnidadProducto) {
		this.valorUnidadProducto = valorUnidadProducto;
	}


	public String getNombreProducto() {
		return nombreProducto;
	}


	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}


	public String getNombreVendedor() {
		return nombreVendedor;
	}


	public void setNombreVendedor(String nombreVendedor) {
		this.nombreVendedor = nombreVendedor;
	}

	
}
